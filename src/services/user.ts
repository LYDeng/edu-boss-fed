/*
 * @Date: 2021-10-21 11:26:16
 * @LastEditors: lyd
 * @LastEditTime: 2021-10-28 16:46:33
 * @Description: 无
 */
import request from '@/utils/request'
import qs from 'qs'

interface User {
  phone: string
  password: string
}

export const login = (data: User) => {
  return request({
    method: 'POST',
    url: '/front/user/login',
    // headers: { 'content-type': 'application/x-www-form-urlencoded' },
    // 如果data是普通对象，则content-type 是application/json
    // 如果用qs.stringify转换之后，则content-type 是application/x-www-form-urlencoded
    // 如果data是 FormData对象，则content-type 是application/json
    data: qs.stringify(data) // axios默认发送的是 application/json 格式数据
  })
}

export const getUserInfo = () => {
  return request({
    method: 'GET',
    url: '/front/user/getInfo'
  })
}

export const getUserPages = (data: any) => {
  return request({
    method: 'POST',
    url: '/boss/user/getUserPages',
    data
  })
}

export const forbidUser = (userId: string | number) => {
  return request({
    method: 'POST',
    url: '/boss/user/forbidUser',
    data: {
      userId
    }
  })
}

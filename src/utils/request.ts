/*
 * @Date: 2021-10-19 17:21:57
 * @LastEditors: lyd
 * @LastEditTime: 2021-10-25 16:24:53
 * @Description: 无
 */

import axios from 'axios'
import store from '@/store'
import { Message } from 'element-ui'
import router from '@/router'
import qs from 'qs'

const request = axios.create({
  // 配置选项
  baseURL: process.env.BASE_API,
  timeout: 5000
})

function redirectToLogin () {
  router.push({
    name: 'login',
    query: {
      redirect: router.currentRoute.fullPath
    }
  })
}

function refreshToken () {
  return axios.create()({
    method: 'POST',
    url: '/front/user/refresh_token',
    data: qs.stringify({
      refreshtoken: store.state.user.refresh_token
    })
  })
}

// request拦截器
request.interceptors.request.use(config => {
  // 在发送请求之前做些什么
  const { user } = store.state
  if (user && user.access_token) {
    config.headers.Authorization = user.access_token
  }
  return config
}, function (error) {
  // 对请求错误做些什么

  return Promise.reject(error)
})
let isRefreshing = false // 控制刷新 token 的状态
let requests: any[] = [] // 存储刷新 token 期间过来的 401 请求
request.interceptors.response.use(function (response) {
  // 状态码为 2xx 都会进入这里
  // 如果是自定义错误状态码，错误处理就写到这里
  // 对响应数据做点什么
  return response
}, async function (error) {
  // 状态码为超出 2xx 都会进入这里
  // 如果是使用http状态码，错误处理就写到这里
  // 对响应错误做点什么
  if (error.response) { // 请求收到了，请求超过 2xx
    const { status } = error.response
    if (status === 400) {
      Message.error('请求参数错误')
    } else if (status === 401) {
      if (!store.state.user) {
        redirectToLogin()
        return Promise.reject(error)
      }
      // 刷新 token
      if (!isRefreshing) {
        isRefreshing = true
        return refreshToken().then(res => {
          if (!res.data.success) {
            throw new Error('刷新 Token 失败')
          }
          // 将取得新token更新到容器中
          store.commit('setUser', res.data.content)
          // 把 requests 队列中的请求重新发出去
          requests.forEach(cb => cb())
          // 重置 requests 数组
          requests = []
          // 将失败请求重新发出去
          return request(error.config)
        }).catch(err => {
          console.log(err)
          store.commit('setUser', null)
          redirectToLogin()
          return Promise.reject(error)
        }).finally(() => {
          isRefreshing = false // 重置刷新状态
        })
      }

      // 刷新状态下，把请求挂起放到 requests 数组中
      return new Promise(resolve => {
        requests.push(() => {
          resolve(request(error.config))
        })
      })
    } else if (status === 403) {
      Message.error('没有权限，请联系管理员')
    } else if (status === 404) {
      Message.error('请求资源不存在')
    }
  } else if (error.request) { // 请求发出去了，没有收到响应
    Message.error('请求超时，请刷新重试')
  } else { // 在设置请求是发生了一些事情，出发了一个错误 。 一般是未知的
    Message.error(`请求失败：${error.message}`)
  }
  return Promise.reject(error)
})

export default request

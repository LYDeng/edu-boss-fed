/*
 * @Date: 2021-10-18 10:48:25
 * @LastEditors: lyd
 * @LastEditTime: 2021-10-19 16:17:13
 * @Description: 无
 */
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import ElementUI from 'element-ui'
// import 'element-ui/lib/theme-chalk/index.css'

// 加载全局样式
import './styles/index.scss'

Vue.use(ElementUI)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')

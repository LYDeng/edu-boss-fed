/*
 * @Date: 2021-10-18 10:48:25
 * @LastEditors: lyd
 * @LastEditTime: 2021-10-27 17:29:30
 * @Description: 无
 */
module.exports = {
  root: true,
  env: {
    node: true
  },
  // 使用插件的编码校验规则
  extends: [
    'plugin:vue/essential',
    '@vue/standard',
    '@vue/typescript/recommended'
  ],
  parserOptions: {
    ecmaVersion: 2020
  },
  // 自定义编码校验规则
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    // 'semi': ['error', 'always']
    '@typescript-eslint/member-delimiter-style': ['error', {
      multiline: {
        delimiter: 'none',
        requireLast: true
      }
    }],
    '@typescript-eslint/no-explicit-any': ['off'],
    //文末需要回车配置不报错设为0， 警告不中断程序为1，报错为2 
    'eol-last': 0,
  }
}
